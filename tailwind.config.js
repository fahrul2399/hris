module.exports = {
   content: [
      "./src/**/*.{html,js,ts,jsx,tsx}",
      "./src/pages/**/*.{html,js,ts,jsx,tsx}",
      "./src/component/**/*.{html,js,ts,jsx,tsx}"
   ],
   theme: {
      extend: {},
   },
   plugins: [],
   prefix: 'tw-',
};
