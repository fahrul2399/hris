import DashboardIcon from "./icon/dashboard.svg";
import UserProfile from "./icon/Vector.svg";
import FaceIcon from "./icon/absensi.svg";
import PendapatanIcon from "./icon/pendapatan.svg";
import SkpIcon from "./icon/skp.svg";
import ValidasiIcon from "./icon/validasi.svg";
import DaftarIcon from "./icon/daftar_pegawai.svg";
import PayrollIcon from "./icon/payroll.svg";
import AbsenIcon from "./icon/data_absen.svg";
import HumanResourceIcon from "./icon/human_resource.svg";
import Logo from "./img/logo.jpeg"
import LogoLong from "./img/logo-long-width.png"
import LogoCollapse from "./img/logo-collapse.png"


export { 
    DashboardIcon, 
    UserProfile,
    FaceIcon,
    PendapatanIcon,
    SkpIcon,
    ValidasiIcon,
    DaftarIcon,
    HumanResourceIcon,
    PayrollIcon,
    AbsenIcon,
    Logo,
    LogoLong,
    LogoCollapse
};


