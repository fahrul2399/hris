import React, { useEffect, useState } from "react";
import { Layout, Menu, Breadcrumb } from "antd";
import ListMenu from "./listmenu";
import { ArrowLeftOutlined, ArrowRightOutlined } from "@ant-design/icons";
import { Logo, LogoLong, LogoCollapse } from "../../assets";
import { Link } from 'react-router-dom';


const { Header, Content, Footer, Sider } = Layout;

const Menus = ({ content }) => {
   const [matches, setMatches] = useState(window.matchMedia("(min-width: 768px)").matches);
   useEffect(() => {
      window.matchMedia("(min-width: 768px)").addEventListener("change", (e) => setMatches(e.matches));
   }, [matches]);
   const [collapse, setCollapse] = useState(false);
   return (
      <Layout hasSider style={{ minHeight: "100vh" }}>
         {matches ? (
            <Sider
               width={266}
               // collapsible
               collapsed={collapse}
               onCollapse={(e) => setCollapse(e)}
               style={{
                  backgroundColor: "#FFFFFF",
                  overflow: "auto",
                  height: "100vh",
                  position: "fixed",
                  left: 0,
                  top: 0,
                  bottom: 0,
                  zIndex: 1,
                  // boxShadow: "1px 1px 1px black",
               }}
            >
               <div 
                  style={{
                     display: "flex",
                     justifyContent: "center"
                  }}
               
               >
                  <Link to="/">
                  <img 
                     style={{
                        width: 'auto',
                        height: 60
                     }}
                     src={ collapse ? LogoCollapse : LogoLong } />
                  </Link>
               </div>
               <ListMenu />
               <div
                  style={{
                     position: "fixed",
                     bottom: 0,
                     backgroundColor: "white",
                     width: collapse ? 80 : 265,
                     display: "flex",
                     alignItems: "center",
                     justifyContent: "center",
                     padding: 10,
                     transition: "all 265ms",
                     // boxShadow: "0px -1px 1px black",
                     cursor: "pointer",
                  }}
                  onClick={() => {
                     setCollapse(!collapse);
                  }}
               >
                  {collapse ? (
                     <ArrowRightOutlined
                        style={{
                           fontSize: 18,
                           fontWeight: "900",
                           color: "black",
                        }}
                     />
                  ) : (
                     <ArrowLeftOutlined
                        style={{
                           fontSize: 18,
                           fontWeight: "900",
                           color: "black",
                        }}
                     />
                  )}
               </div>
            </Sider>
         ) : (
            <Sider
               collapsible
               collapsed={collapse}
               breakpoint="lg"
               collapsedWidth="0"
               reverseArrow={true}
               style={{
                  backgroundColor: "#1E1F21",
                  paddingTop: 50,
                  overflow: "auto",
                  height: "100vh",
                  position: "fixed",
                  left: 0,
                  top: 0,
                  bottom: 0,
                  zIndex: 1,
               }}
            >
               <ListMenu />
            </Sider>
         )}
         <Layout
            className="site-layout"
            style={{
               marginLeft: matches ? (collapse ? 80 : 265) : 0,
               transition: "all 280ms",
            }}
         >
            <Header
               className="site-layout-background"
               style={{
                  padding: 0,
                  backgroundColor: "white",
               }}
            />
            <Content
               style={{
                  margin: "0 16px",
               }}
            >
               <div
                  className="site-layout-background"
                  style={{
                     padding: 24,
                     minHeight: 360,
                  }}
               >
                  {content}
               </div>
            </Content>
            <Footer
               style={{
                  textAlign: "center",
               }}
            ></Footer>
         </Layout>
      </Layout>
   );
};

export default Menus;
