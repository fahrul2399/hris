import React, { useState, useEffect } from "react";
import { LineChartOutlined, DatabaseOutlined, TagOutlined } from "@ant-design/icons";
import { DashboardIcon } from "../../assets";
import { Menu } from "antd";
import { Link } from "react-router-dom";
import { color, font } from "../../config/styles";
import axios from 'axios'

const { SubMenu } = Menu;


const ListMenu = () => {
   const [backgroundMenu, setBackgroundMenu] = useState(null);

   const [routeData, setRouteData] = useState([])

   const getMenu = async () => {
      try {
         const response = await axios.get("http://172.16.35.90/app/menu")
         const data = response.data
         setRouteData(data)
      } catch (error) {
         console.log(error)
      }
      
   }

   useEffect(() => {
      getMenu()
   },[])

   return (
      <Menu
         defaultSelectedKeys={["4"]}
         mode="inline"
         defaultOpenKeys={["sub4"]}
         theme="light"
         style={{
            backgroundColor: "#FFFFFF",
            paddingBottom: 25,
         }}
      >
         
         <hr
            style={{
               marginLeft: 30,
               marginRight: 30,
               marginTop: 10,
               marginBottom: 30
            }}
         />
         {/* Custome menu */}
         {
            routeData.map(menu => (
               menu.children ?
               ( 
                  <SubMenu
                     title={menu.label}
                     icon={
                        <LineChartOutlined
                           style={{
                              fontWeight: "bold",
                              color: "#5E5E61",
                              fontSize: font.big,
                           }}
                        />
                     }
                     key={menu.id}
                     style={{
                        fontSize: font.medium,
                        fontWeight: "600",
                        color: "#5E5E61",
                        backgroundColor: backgroundMenu === menu.id ? "#E59546" : "#FFFFFF",
                        borderRadius: 5,
                        margin: 5,
                     }}
                  >

                     {
                        menu.children.map((subMenu, index) => (
                           
                           subMenu.children ? 
                              (
                                 <SubMenu
                                    title={subMenu.label}
                                    icon={
                                       <LineChartOutlined
                                          style={{
                                             fontWeight: "bold",
                                             color: "#5E5E61",
                                             fontSize: font.big,
                                          }}
                                       />
                                    }
                                    key={subMenu.id}
                                    style={{
                                       fontSize: font.medium,
                                       fontWeight: "600",
                                       color: "#5E5E61",
                                       backgroundColor: backgroundMenu === subMenu.id ? "#E59546" : "#FFFFFF",
                                       borderRadius: 5,
                                       margin: 5,
                                    }}
                                 >
                                    {
                                       subMenu.children.map(menuItem => (
                                          <Menu.Item
                                             onClick={() => setBackgroundMenu(menuItem.id)}
                                             key={menuItem.id}
                                             style={{
                                                background: backgroundMenu === menuItem.id ? "#E59546" : "#FFFFFF",
                                                borderRadius: 5,
                                                margin: 5,
                                             }}
                                             icon={
                                                <TagOutlined
                                                   style={{
                                                      color: "#5E5E61",
                                                   }}
                                                />
                                             }
                                          >
                                             <Link
                                                to={menuItem.route}
                                                style={{
                                                   color: "#5E5E61",
                                                   fontSize: font.medium,
                                                }}
                                             >
                                                {menuItem.label}
                                             </Link>
                                          </Menu.Item>
                                       ))
                                    }
                                 </SubMenu>              
                              )
                           : 
                           (
                              <Menu.Item
                                 onClick={() => setBackgroundMenu(subMenu.id)}
                                 key={subMenu.id}
                                 style={{
                                    background: backgroundMenu === subMenu.id ? "#E59546" : "#FFFFFF",
                                    borderRadius: 5,
                                    margin: 5,
                                 }}
                                 icon={
                                    <TagOutlined
                                       style={{
                                          color: "#5E5E61",
                                       }}
                                    />
                                 }
                              >
                                 <Link
                                    to={subMenu.route}
                                    style={{
                                       color: "#5E5E61",
                                       fontSize: font.medium,
                                    }}
                                 >
                                    {subMenu.label}
                                 </Link>
                              </Menu.Item>     
                           )       
                        ))
                     }   
                  </SubMenu>
               )
               :
               menu.label &&
               (
                  <Menu.Item
                  onClick={() => {
                     setBackgroundMenu(menu.id);
                  }}
                  icon={
                     <img
                        src={DashboardIcon}
                        style={{
                           color: backgroundMenu === menu.id ? "#FFFFFF" : "#5E5E61",
                           width: 20,
                        }}
                     />
                  }
                  style={{
                     fontSize: font.medium,
                     fontWeight: "600",
                     backgroundColor: backgroundMenu === menu.id ? "#E59546" : "#FFFFFF",
                     borderRadius: 5,
                     margin: 5,
                  }}
                  key={menu.id}
                  >
                  <Link
                     to={menu.route}
                     style={{
                        color: backgroundMenu === menu.id ? "#FFFFFF" : "#5E5E61",
                        fontSize: font.medium,
                     }}
                  >
                     {menu.label}
                  </Link>
                  </Menu.Item>
               )
            )
            )
         }   
      </Menu>
   );
};

export default ListMenu;
