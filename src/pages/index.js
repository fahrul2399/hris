import Dashboard from "./dashboard";

import { Kehadiran, KetidakHadiran, Lembur, Perjalanan } from "./attendance";
import { BalanceScoreCard, Evaluasi, KeyPerformanceIndicator, PenilaianKinerja, PerencanaanKinerja } from "./performance";

export { Dashboard, BalanceScoreCard, Evaluasi, KeyPerformanceIndicator, PenilaianKinerja, PerencanaanKinerja, Kehadiran, KetidakHadiran, Lembur, Perjalanan };
