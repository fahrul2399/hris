import React, { useEffect, useState } from "react";
import { Menus } from "../component";
import { Route, Routes } from "react-router-dom";
import { RouteAttandance, RouteDashboard, RoutePerformance } from "../routes/firstroute";
import CustomePage from "./customePage";
import CustomeRoute from "../routes/customeroute";
import axios from "axios";

const Layout = ({routeData}) => {

   return (
      <div>
         <Menus
            content={
               <CustomeRoute routeData={routeData} />
            }
         />
      </div>
   );
};
export default Layout;
