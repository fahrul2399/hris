import React, { useEffect, useState } from "react";
import axios from 'axios';
import $ from 'jquery'
import '../../index.css';

const CustomePage = (props) => {
    const { url, label } = props

    const [data, setData] = useState(null)
    const [scriptCount, setScriptCount] = useState(0)
    
    const getData = async () => {
        try {
            let response = await axios.get(url)
            const renderHtml = await response.data
            setData(renderHtml)
            return renderHtml
            
        } catch (error) {
            console.log(error)
        }
    }
    function createMarkup() {
        return {__html: `${data}`}
    }

    let count = 0
    useEffect(() => {
        
        getData()
        .then((ajaxText) => {            
            let ajaxScript = ajaxText.substring(ajaxText.indexOf('<script>') + 8, ajaxText.indexOf('</script>'))
            const script = document.createElement('script');
            const scriptId = document.getElementById(`script-1`)

            if(scriptId) {
                document.body.removeChild(scriptId)
            }
            script.async = true;
            script.type='text/javascript';
            script.id = `script-${count}`
            script.innerHTML = ajaxScript;
            if(count === 0){
                document.body.appendChild(script);
            }
            count++
        })
        return () => {
            const scripById = document.getElementById(`script-0`)
            const scripById2 = document.getElementById(`script-1`)
            if(scripById) {
                document.body.removeChild(scripById)
            }
            if(scripById2) {
                document.body.removeChild(scripById2)
            }
        }
        
    },[url])

    return (
        <>
            <div id="body" dangerouslySetInnerHTML={createMarkup()}>

            </div>

        </>
        
    );
    
};

export default CustomePage;

