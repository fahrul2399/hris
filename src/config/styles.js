export const color = {
  dark: "darkblue",
  light: "ligtblue",
};

export const font = {
  small: 12,
  medium: 13,
  big: 18,
};
