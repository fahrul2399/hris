import axios from 'axios';
export default async function API(props){
    const {headers, url, method, data} = props

    const response = await axios({
        url,
        method,
        data,
        headers
    }).catch((err) => err.response);

    return response

}