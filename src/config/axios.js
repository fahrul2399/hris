import React from "react";
import axios from "axios";

export const axiosGet = (url) => {
  try {
    const res = axios.get(url);
    return res;
  } catch (err) {
    console.log(err);
  }
};

export const axiosDelete = (val) => {
  try {
    const res = axios.delete(val);
    return res;
  } catch (err) {
    console.log(err);
  }
};

export const axiosUpdate = (val) => {
  try {
    const res = axios.put(val.url, val.data);
    return res;
  } catch (err) {
    console.log(err);
  }
};

export const axiosPost = (val) => {
  try {
    const res = axios.post(val.url, val.data);
    return res;
  } catch (err) {
    console.log(err);
  }
};
