import { combineReducers } from "redux";

import { reducerGlobal, sagaGlobal } from "./global";

import { spawn } from "redux-saga/effects";

export const allReducer = combineReducers({
   reducerGlobal,
});

export function* mySaga() {
   yield spawn(sagaGlobal);
}
