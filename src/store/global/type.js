const prefix = "global";

export const LOADING = `${prefix}/setloading`;
export const ERROR = `${prefix}/error`;
export const STYLE = `${prefix}/style`;

const prefixSaga = "mysaga";

export const SAGA_GLOBAL = `${prefixSaga}/global`;
