import * as type from "./type";
import { put, takeEvery } from "redux-saga/effects";

function* setLoading() {
  yield put({ type: type.LOADING, value: true });
}

function* clearLoading() {
  yield put({ type: type.LOADING, value: false });
}

function* setGlobalError(action) {
  yield put({
    type: type.ERROR,
    status: action.payload.status,
    message: action.payload.message,
  });
}

function* clearGlobalError() {
  yield put({ type: type.ERROR, status: null, message: null });
}

export function* sagaGlobal() {
  yield takeEvery(type.SAGA_GLOBAL + "/setloading", setLoading);
  yield takeEvery(type.SAGA_GLOBAL + "/clearloading", clearLoading);
  yield takeEvery(type.SAGA_GLOBAL + "/error", setGlobalError);
  yield takeEvery(type.SAGA_GLOBAL + "/clearError", clearGlobalError);
}
