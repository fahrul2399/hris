import * as type from "./type";

const initialState = {
  error: {
    status: null,
    message: null,
  },
  loading: false,
  style: {
    fontsize: 13,
  },
};

export const reducerGlobal = (state = initialState, action) => {
  switch (action.type) {
    case type.LOADING:
      return {
        ...state,
        loading: action.value,
      };
    case type.ERROR:
      return {
        ...state,
        error: {
          status: action.status,
          message: action.message,
        },
      };
    case type.STYLE:
      return {
        ...state,
        style: {
          ...state.style,
          fontsize: action.fontsize,
        },
      };
    default:
      return state;
  }
};
