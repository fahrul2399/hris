import { sagaGlobal } from "./action";
import { reducerGlobal } from "./reducer";

export { sagaGlobal, reducerGlobal };
