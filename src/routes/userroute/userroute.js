import React from "react";
import { Route, Routes } from "react-router-dom";
import Layout from "../../pages/layout";
import CustomePage from './../../pages/customePage/index';

const UserRoute = ({routeData}) => {

   return (
      <Routes>
         <Route path="/*" element={<Layout routeData={routeData}/>} />
      </Routes>
   );
};

export default UserRoute;
