import React from "react";
import { Route, Routes } from "react-router-dom";
import { BalanceScoreCard, Dashboard, Evaluasi, KeyPerformanceIndicator, PenilaianKinerja, PerencanaanKinerja } from "../../pages";

const RoutePerformance = () => {
   return (
      <Routes>
         <Route path="/balancescorecard" element={<BalanceScoreCard />} />
         <Route path="/keyperformanceindicator" element={<KeyPerformanceIndicator />} />
         <Route path="/evaluasi" element={<Evaluasi />} />
         <Route path="/perencanaankinerja" element={<PerencanaanKinerja />} />
         <Route path="/penilaiankinerja" element={<PenilaianKinerja />} />
      </Routes>
   );
};
export default RoutePerformance;
