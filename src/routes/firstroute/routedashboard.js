import React from "react";
import { Route, Routes } from "react-router-dom";
import { Dashboard } from "../../pages";

const RouteDashboard = () => {
  return (
    <Routes>
      <Route path="/" element={<Dashboard />} />
    </Routes>
  );
};
export default RouteDashboard;
