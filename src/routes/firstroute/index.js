import RouteDashboard from "./routedashboard";
import RouteAttandance from "./routeAttandance";
import RoutePerformance from "./routePerformance";

export { RouteDashboard, RouteAttandance, RoutePerformance };
