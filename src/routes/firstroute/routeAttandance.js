import React from "react";
import { Route, Routes } from "react-router-dom";
import { Kehadiran, KetidakHadiran, Lembur, Perjalanan } from "../../pages";

const RouteAttandance = () => {
   return (
      <Routes>
         <Route path="/kehadiran" element={<Kehadiran />} />
         <Route path="/ketidakhadiran" element={<KetidakHadiran />} />
         <Route path="/lembur" element={<Lembur />} />
         <Route path="/perjalanan" element={<Perjalanan />} />
      </Routes>
   );
};
export default RouteAttandance;
