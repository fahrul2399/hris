import React, {useState, useEffect} from "react";
import { Route, Routes } from "react-router-dom";
import CustomePage from './../../pages/customePage/index';
import Data from "../../data/menu.json"
import Dashboard from './../../pages/dashboard/index';
import axios from "axios";
import DataTable from 'datatables.net';

const CustomeRoute = ({routeData}) => {

    return (
        <Routes>
            {
                routeData.map(menuItem => (
                    <Route path={`${menuItem.route}`} 
                        element={
                            <CustomePage 
                                url={menuItem.url} 
                                label={menuItem.label} 
                            />
                        }/>
                ))
            }    
        </Routes>
    );
};
export default CustomeRoute;
