import "./App.css";
import React, {useEffect, useState} from "react";
import { BrowserRouter } from "react-router-dom";
import { createStore, applyMiddleware } from "redux";
import UserRoute from "./routes/userroute/userroute";
import { Provider } from "react-redux";
import { allReducer, mySaga } from "./store";
import createSagaMiddleware from "redux-saga";
import axios from "axios";
const sagaMiddleware = createSagaMiddleware();
const store = createStore(allReducer, applyMiddleware(sagaMiddleware));
store.subscribe(() => console.log("subcribe", store.getState()));
sagaMiddleware.run(mySaga);

const App = () => {

    const [routeData, setRouteData] = useState([])

    const getMenu = async () => {
        try {
            const response = await axios.get("http://172.16.35.90/app/menu")
            const data = response.data
            let tempRoute = []

            data.forEach(menu => {
              if(menu.children){
                  menu.children.forEach(menuItem => {
                      if(menuItem.children){
                          menuItem.children.forEach(subMenu => {
                            tempRoute.push({"url" : subMenu.url, "label": subMenu.label, "route": subMenu.route })    
                          })
                      }else {
                          tempRoute.push({"url" : menuItem.url, "label": menuItem.label, "route": menuItem.route })    
                      }
                  })        
              }else {
                  tempRoute.push({"url" : menu.url, "label": menu.label, "route": menu.route })
              }
            })
            setRouteData(tempRoute)            
        } catch (error) {
            console.log(error)
        }

    }

    useEffect(() => {
      getMenu()
    },[])

  return (
    <Provider store={store}>
      <BrowserRouter>
        <UserRoute routeData={routeData} />
      </BrowserRouter>
    </Provider>
  );
};

export default App;
